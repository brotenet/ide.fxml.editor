package ide.fxml.editor;

import api.commons.Environment;
import api.commons.WindowManager;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.stage.Window;

public class Main extends Application {
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		Window window = WindowManager.newWindow("/ide/fxml/editor/FxmlEditor.fxml", "/resources/application.css");
		WindowManager.setResizable(window, true);
		WindowManager.setMaximized(window, true);
		WindowManager.setTitle(window, "FXML Editor");
		WindowManager.setImage(window, "/resources/application.png");
		WindowManager.show(window);
	}
	
	public static void main(String[] args) {
		Environment.Session.putProperties(args);
		if(!Environment.Session.getProperties().containsKey("--file")) {
			System.out.println("Auto-setting --file=" + args[0]);
			Environment.Session.putProperty("--file", args[0]);
		}
		launch(args);
	}
}

