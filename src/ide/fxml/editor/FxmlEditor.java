package ide.fxml.editor;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.imageio.ImageIO;

import org.json.JSONObject;

import com.oracle.javafx.scenebuilder.kit.editor.EditorController;
import com.oracle.javafx.scenebuilder.kit.editor.EditorController.ControlAction;
import com.oracle.javafx.scenebuilder.kit.editor.EditorController.EditAction;
import com.oracle.javafx.scenebuilder.kit.editor.panel.content.ContentPanelController;
import com.oracle.javafx.scenebuilder.kit.editor.panel.css.CssPanelController;
import com.oracle.javafx.scenebuilder.kit.editor.panel.hierarchy.HierarchyPanelController;
import com.oracle.javafx.scenebuilder.kit.editor.panel.inspector.InspectorPanelController;
import com.oracle.javafx.scenebuilder.kit.editor.panel.library.LibraryPanelController;
import com.oracle.javafx.scenebuilder.kit.library.LibraryItem;
import com.oracle.javafx.scenebuilder.kit.preview.PreviewWindowController;

import api.commons.Controller;
import api.commons.Environment;
import api.commons.Environment.FileSystem;
import api.commons.Environment.Resources;
import api.commons.WindowManager;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class FxmlEditor extends Controller{

	EditorController editor_controller;
	ContentPanelController content_panel_controller;
	LibraryPanelController library_panel_controller;
	HierarchyPanelController hierarchy_panel_controller;
	InspectorPanelController inspector_panel_controller;
	PreviewWindowController preview_window_controller;
	CssPanelController css_panel_controller;
	
	Stage css_stage;
	
	File file;
	String recycle_fxml;
	
	@FXML AnchorPane root;
	@FXML SplitPane spMaster, spLeft;
	@FXML TextField txtFilter;
	@FXML ToolBar toolbar;
	@FXML MenuButton edit_file_name;
	@FXML MenuItem edit_file_controller;
	
	public FxmlEditor() {
		try {
			editor_controller = new EditorController();
			List<String> section_excludes = Arrays.asList(new String[] {"gluon"});
			List<String> component_excludes = Arrays.asList(new String[] {"mediaview"});
			for(LibraryItem item : editor_controller.getLibrary().getItems().toArray(new LibraryItem[editor_controller.getLibrary().getItems().size()])) {
				if(section_excludes.contains(item.getSection().toLowerCase().trim()) || component_excludes.contains(item.getName().toLowerCase().trim())) {
					editor_controller.getLibrary().getItems().remove(item);
				}
			}
			if(Environment.Session.getProperties().containsKey("--load-extensions")) {
				if(Environment.FileSystem.check(Environment.Session.getProperty("--load-extensions"))) {
					JSONObject extensions = new JSONObject(FileSystem.cat(Environment.Session.getProperty("--load-extensions")));
					System.out.println("using " + Environment.Session.getProperty("--load-extensions"));
					for(JSONObject item : extensions.getJSONArray("controls").toArray(JSONObject.class)) {
						LibraryItem library_item = new LibraryItem(item.getString("description"), item.getString("category"), makeFxmlText(Class.forName(item.getString("class_path"))), getClass().getResource(item.getString("image")), editor_controller.getLibrary());
						editor_controller.getLibrary().getItems().add(library_item);					
					}
				}else {
					System.out.println(Environment.Session.getProperty("--load-extensions") + " file not found.");
				}
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}
	
	@Override
	public void onWindowShowing(WindowEvent event) {
		content_panel_controller = new ContentPanelController(editor_controller);
		library_panel_controller = new LibraryPanelController(editor_controller, null);
		hierarchy_panel_controller = new HierarchyPanelController(editor_controller);
		inspector_panel_controller = new InspectorPanelController(editor_controller);
		spLeft.getItems().add(library_panel_controller.getPanelRoot());
		spLeft.getItems().add(hierarchy_panel_controller.getPanelRoot());
		spMaster.getItems().add(content_panel_controller.getPanelRoot());
		spMaster.getItems().add(inspector_panel_controller.getPanelRoot());
	}
	
	@Override
	public void onWindowShown(WindowEvent event) {
		spMaster.setDividerPositions(0.1, 0.8, 0.1);
		spLeft.setDividerPositions(0.6, 0.4);		
		try {
			file = new File(Environment.Session.getProperty("--file"));
			recycle_fxml = Environment.FileSystem.cat(file.getAbsolutePath());
			System.out.println("Editing file: " + file.getAbsolutePath().toString());
			editor_controller.setFxmlLocation(file.toURI().toURL());
			editor_controller.setFxmlText(recycle_fxml, false);
			
			edit_file_name.setText(file.getName());
			edit_file_controller.setText(editor_controller.getFxomDocument().getFxomRoot().getFxController());
			
		} catch (IOException exception) {
			WindowManager.Dialogs.showExceptionDialog(exception, WindowManager.windowOf(root));
		}
	}
	
	@FXML private void controllerclass() {
		try {
			String output = WindowManager.Dialogs.showTextInputDialog("Please provide a valid controller class.", "Controller class path: ", "Controller Class", editor_controller.getFxomDocument().getFxomRoot().getFxController(), WindowManager.windowOf(root));
			if(output != null) {
				editor_controller.getFxomDocument().getFxomRoot().setFxController(output.trim());
				edit_file_controller.setText(output.trim());
			}
		} catch (IOException exception) {
			WindowManager.Dialogs.showExceptionDialog(exception, WindowManager.windowOf(root));
		}
	}
	
	@FXML private void filter() {
		library_panel_controller.setSearchPattern(txtFilter.getText());
	}
	
	@FXML private void preview() {
		preview_window_controller = new PreviewWindowController(editor_controller, (Stage) WindowManager.windowOf(root));
		preview_window_controller.getStage().initModality(Modality.APPLICATION_MODAL);
		Scene scene = preview_window_controller.getScene();
		Stage stage = new Stage();
		stage.setWidth(800);
		stage.setHeight(600);
		stage.initOwner(WindowManager.windowOf(root));
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.setAlwaysOnTop(true);
		stage.setScene(scene);
		stage.setTitle("Preview Mode");
		stage.getIcons().add(Resources.getImage("/resources/preview.png"));
		stage.showAndWait();
		stage.close();
	}
	
	@FXML private void save() {
		try {
			Environment.FileSystem.touch(file.getAbsolutePath(), editor_controller.getFxmlText(),true);
		} catch (IOException exception) {
			WindowManager.Dialogs.showExceptionDialog(exception, WindowManager.windowOf(root));
		}
	}
	
	@FXML private void undo() {
		if(editor_controller.canUndo()) {
			editor_controller.undo();
		}		
	}
	
	@FXML private void redo() {
		if(editor_controller.canRedo()) {
			editor_controller.redo();
		}		
	}
	
	@FXML private void recycle() {
		try {
			String result = WindowManager.Dialogs.showComboSelectionDialog("Please confirm that you wish to recycle the scene\nprogress and reset all the latest changes made.", new String[] {"Do Not Recycle", "Recycle"}, 0, WindowManager.windowOf(root));
			if(result.equalsIgnoreCase("Recycle")) {
				editor_controller.setFxmlText(recycle_fxml, false);
				edit_file_controller.setText(editor_controller.getFxomDocument().getFxomRoot().getFxController());
			}
		} catch (IOException exception) {
			WindowManager.Dialogs.showExceptionDialog(exception, WindowManager.windowOf(root));
		}
	}
	
	private void createCssStage() {
		css_panel_controller = new CssPanelController(editor_controller, null);
		css_stage = new Stage();
		css_stage.setScene(new Scene(css_panel_controller.getPanelRoot()));
		css_stage.setWidth(800);
		css_stage.setHeight(600);
		css_stage.setResizable(true);
		css_stage.initOwner(WindowManager.windowOf(root));
		css_stage.setAlwaysOnTop(true);
		css_stage.setTitle("CSS Styles View");
		css_stage.getIcons().add(Resources.getImage("/resources/css.png"));
	}
	
	@FXML private void css() {
		if(css_stage == null) {
			createCssStage();
		}
		css_stage.show();
	}
	
	@FXML private void snapshot() {
		String target_path = null;
		if(Environment.Session.getProperty("--snapshot-dir") != null) {
			if(Environment.Session.getProperty("--snapshot-dir").trim().length() > 0) {
				target_path = Environment.Session.getProperty("--snapshot-dir").trim() + Environment.Session.FileSeparator() + file.getName() + ".png";
			}
		}
		if(target_path == null) {
			DirectoryChooser dialog = new DirectoryChooser();
			dialog.setTitle("Select output directory");
			target_path = dialog.showDialog(WindowManager.windowOf(root)).getAbsolutePath() + Environment.Session.FileSeparator() + file.getName() + ".png";
		}
		WritableImage image;
		if(target_path != null) {
			if(Environment.Session.getProperty("--snapshot-size") != null) {
				ImageView resizer = new ImageView(content_panel_controller.getContentSubScene().snapshot(new SnapshotParameters(), null));
				resizer.setPreserveRatio(true);
				resizer.setFitWidth(Integer.valueOf(Environment.Session.getProperty("--snapshot-size").split("x")[0].trim()));
				resizer.setFitHeight(Integer.valueOf(Environment.Session.getProperty("--snapshot-size").split("x")[1].trim()));
				image = resizer.snapshot(null, null);
			}else {
				image = content_panel_controller.getContentSubScene().snapshot(new SnapshotParameters(), null);
			}
			try {
				ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", new File(target_path));
				WindowManager.Dialogs.showAlertDialog("Image snapshot was saved at " + target_path, Environment.Resources.getImage("/resources/camera_48.png"), WindowManager.windowOf(root));
			} catch (IOException exception) {
				WindowManager.Dialogs.showExceptionDialog(exception, WindowManager.windowOf(root));
			}
		}
	}
	
	private static final KeyCodeCombination event_undo = new KeyCodeCombination(KeyCode.Z, KeyCodeCombination.CONTROL_DOWN);
	private static final KeyCodeCombination event_redo = new KeyCodeCombination(KeyCode.Z, KeyCodeCombination.CONTROL_DOWN, KeyCodeCombination.SHIFT_DOWN);
	private static final KeyCodeCombination event_recycle = new KeyCodeCombination(KeyCode.R, KeyCodeCombination.CONTROL_DOWN, KeyCodeCombination.SHIFT_DOWN);
	private static final KeyCodeCombination event_save = new KeyCodeCombination(KeyCode.S, KeyCodeCombination.CONTROL_DOWN);
	private static final KeyCodeCombination event_preview = new KeyCodeCombination(KeyCode.F5);
	private static final KeyCodeCombination event_css = new KeyCodeCombination(KeyCode.F6);
	private static final KeyCodeCombination event_delete = new KeyCodeCombination(KeyCode.DELETE);
	private static final KeyCodeCombination event_copy = new KeyCodeCombination(KeyCode.C, KeyCodeCombination.CONTROL_DOWN);
	private static final KeyCodeCombination event_paste = new KeyCodeCombination(KeyCode.V, KeyCodeCombination.CONTROL_DOWN);
	
	@FXML private void keyboardCommands(KeyEvent event) {
		if(event_undo.match(event)) {
			undo();
		}else if(event_redo.match(event)) {
			redo();
		}else if(event_recycle.match(event)) {
			recycle();
		}else if(event_save.match(event)) {
			save();
		}else if(event_preview.match(event)) {
			preview();
		}else if(event_css.match(event)) {
			css();
		}else if(event_delete.match(event)) {
			editor_controller.performEditAction(EditAction.DELETE);
		}else if(event_copy.match(event)) {
			editor_controller.performControlAction(ControlAction.COPY);
		}else if(event_paste.match(event)) {
			editor_controller.performEditAction(EditAction.PASTE);
		}
	}
	
	private static String makeFxmlText(Class<?> componentClass) {
		final StringBuilder sb = new StringBuilder();
		sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		sb.append("<?import ");
		sb.append(componentClass.getCanonicalName());
		sb.append("?>");
		sb.append("<");
		sb.append(componentClass.getSimpleName());
		sb.append("/>\n");
		return sb.toString();
	}
}
